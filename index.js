const express = require("express");
const app = express();

app.get("/", (req, res) => {
    res.send({hello: "world"});
});

app.get("/sum", (req, res) => {
    console.log(req)
    const numA = parseInt(req.query.numA);
    const numB = parseInt(req.query.numB);
    const result = numA + numB;
    res.send({sum: result});

})

const PORT = process.env.PORT || 5000;
app.listen(PORT, function() {
    console.log(`...... ${PORT}`);
})
